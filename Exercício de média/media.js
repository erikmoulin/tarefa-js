// Faça um algoritmo em que você recebe 3 notas de um aluno e caso a
// média aritmética dessas notas for maior ou igual que 6 imprima
// “Aprovado”, caso contrário “Reprovado”.

function mediaAluno(n1, n2, n3) {
    sum = (n1 + n2 + n3) / 3
    if (sum >= 6) {
        console.log("Aprovado")
    } else {
        console.log("Reprovado")
    }
    return sum
} 


let nota1 = Math.floor(Math.random() * 10)

let nota2 = Math.floor(Math.random() * 10)

let nota3 = Math.floor(Math.random() * 10)

var media = (mediaAluno(nota1, nota2, nota3)).toFixed(1)

console.log(`A primeira nota foi ${nota1}, a segunda ${nota2} e a terceira ${nota3}. Sua média foi ${media}.`)