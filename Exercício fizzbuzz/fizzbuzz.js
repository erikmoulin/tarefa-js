// Teste 5 números inteiros aleatórios. Os testes:
// Caso o valor seja divisível por 3, imprima no console “fizz”.
// Caso o valor seja divisível por 5 imprima “buzz”.
// Caso o valor seja divisível por 3 e 5, ao mesmo tempo, imprima
// “fizzbuzz”.
// Caso contrário imprima nada.

function fizzbuzz (numero) {
    if (numero % 3 == 0 && numero % 5 == 0) {
        return 'fizzbuzz';
    } else if (numero % 3 == 0) {
        return 'fizz';
    } else if (numero % 5 == 0) {
        return 'buzz';
    } else {
        return ''
    }
}

function numerosAleatorios() {
    return Math.floor(Math.random() * 100)
}


var max = 0

while (max < 5) {
     console.log(fizzbuzz(numerosAleatorios()))
    max = max + 1
}