// Escreva um algoritmo para ler uma temperatura em graus Fahrenheit,
// calcular e escrever o valor correspondente em graus Celsius (baseado na
// fórmula abaixo):
// 𝐶/5 = ( 𝑓 − 32)/9

function mudaTemp(temperaturaEmFahrenheit) {
        var cel = ((temperaturaEmFahrenheit - 32) / 9) * 5
        return cel.toFixed(2)
}

var numeroRandom = (Math.random() * 100).toFixed(2);

temp = mudaTemp(numeroRandom)

  console.log(`A temperatura ${numeroRandom} em graus Fahrenheit, equivale a ${temp} graus Celsius`)